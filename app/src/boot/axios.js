
import { boot } from 'quasar/wrappers'
import axios from 'axios'
import store from '../store'
const api = axios.create({ baseURL: 'http://localhost:3000' })

api.interceptors.request.use(async req => {
  const token = store.getters.getToken()
  req.headers.Authorization = `Bearer ${token}`
  return req
})

// Response interceptor for API calls
api.interceptors.response.use((response) => {
  return response
}, async function (error) {
  const originalRequest = error.config
  if (error.response.status === 403 && !originalRequest._retry) {
    originalRequest._retry = true
    const accessToken = await store.methods.refreshToken()
    store.methods.updateToken(accessToken.data.accessToken)
    return api(originalRequest)
  }
  return Promise.reject(error)
})

export default boot(({ app }) => {
  // for use inside Vue files (Options API) through this.$axios and this.$api

  app.config.globalProperties.$axios = axios
  // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
  //       so you won't necessarily have to import axios in each vue file

  app.config.globalProperties.$api = api
  // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)
  //       so you can easily perform requests against your app's API
})

export { axios, api }
